#!/usr/bin/env bash

pip3 install pyarrow --user
pip3 install jira --user
pip3 install python-dotenv --user

airflow initdb

airflow webserver