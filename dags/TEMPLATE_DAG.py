from airflow import DAG

from airflow.operators.postgres_operator import PostgresOperator
from airflow.operators.python_operator import PythonOperator
from airflow.operators.redshift_to_s3_operator import RedshiftToS3Transfer
from airflow.operators.s3_to_redshift_operator import S3ToRedshiftTransfer

from airflow.hooks.postgres_hook import PostgresHook
from airflow.hooks.S3_hook import S3Hook

import os
import boto3
from sqlalchemy import create_engine
import pandas as pd

from airflow.utils.dates import days_ago
from datetime import datetime, timedelta

#Local Dev Only
from dotenv import load_dotenv
load_dotenv()

example_sql = "Select * from table;"

# These are configuration settings for the dag
default_args = {
    'owner': 'firstname.lastname',
    'depends_on_past': False,
    'start_date': days_ago(1),
    'email': ['airflow@example.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 0,
    'retry_delay': timedelta(minutes=3)
}

# Here we are initializing the dag and setting how it will run
dag = DAG(
    dag_id='template_dag',
    schedule_interval='0 */4 * * *',
    dagrun_timeout=timedelta(minutes=120),
    default_args=default_args,
    max_active_runs=1,
    concurrency=8,
    catchup=False
)

### Everything below gives the dag various tasks

example_postgres = PostgresOperator(
    task_id='run_sql_query',
    sql='''
    SELECT * FROM table;
    ''',
    postgres_conn_id='REDSHIFT_OMEGA_ANALYSIS', 
    dag=dag
    )

example_RedshiftToS3 = RedshiftToS3Transfer(
  task_id='push_table_to_S3',
  schema='public',
  table='table_name',
  s3_bucket='olive-omega-s3-bucket',
  s3_key='path/to/folder',
  redshift_conn_id='REDSHIFT_METRICS',
  aws_conn_id='AWS_DEFAULT',
  unload_options = ['FORMAT AS PARQUET', "KMS_KEY_ID '406d180f-53ad-426e-ac51-e05af7df781b'", "ENCRYPTED", "ALLOWOVERWRITE"],
  dag=dag
)

example_S3ToRedshift = S3ToRedshiftTransfer(
    task_id='copy_file_from_S3_to_Redshift',
    s3_bucket='olive-omega-s3-bucket',
    s3_key='path/to/folder',
    schema="PUBLIC",
    table='file_name',
    redshift_conn_id='REDSHIFT_OMEGA_ANALYSIS',
    copy_options=['FORMAT AS PARQUET'],
    dag=dag
)

[example_postgres, example_RedshiftToS3] >> example_S3ToRedshift

#You can have airflow run python functions
def example_python_function(**kwargs):

    # How to pull in an airflow redshift connection
    redshift_conn_id = kwargs.get('redshift_conn_id')
    postgres_hook = PostgresHook(postgres_conn_id=redshift_conn_id)
    
    postgres_hook.run(example_sql) # Runs query
    postgres_hook.get_pandas_df(example_sql) #R uns query and returns pandas df

    # How to use sqlalchemy to pull in redshift data
    metrics_URI = os.getenv('AIRFLOW_CONN_REDSHIFT_METRICS')
    engine = create_engine(metrics_URI)
    engine_data = pd.read_sql_query(example_sql, engine)


    # How to pull in the airflow AWS connection to grab the credentials
    aws_conn_id = kwargs.get('aws_conn_id')
    s3_hook = S3Hook(aws_conn_id=aws_conn_id, verify=None)
    credentials = s3_hook.get_credentials()
    s3_client = boto3.client('s3', 
                      aws_access_key_id=credentials.access_key, 
                      aws_secret_access_key=credentials.secret_key
                      )

    # How to pull in specific variables from your .env file
    aws_access_key_id = os.getenv("aws_access_key_id")
    aws_secret_access_key = os.getenv("aws_secret_access_key")
    s3_client = boto3.client('s3', 
                      aws_access_key_id=aws_access_key_id, 
                      aws_secret_access_key=aws_secret_access_key
                      )

    # How to run an operator inside a function
    PostgresOperator(
        task_id='run_operator_inside_function',
        sql=example_sql,
        postgres_conn_id='REDSHIFT_OMEGA_ANALYSIS', 
    ).execute(context=None)

example_python = PythonOperator(
    task_id='run_python_function',
    # Feed your function specific inputs
    op_kwargs = {'redshift_conn_id':'REDSHIFT_OMEGA_ANALYSIS',
                'aws_conn_id':'AWS_DEFAULT',}, 
    python_callable = example_python_function,
    dag=dag)